<?php

namespace App\Traits;

trait EntityTableNameTrait
{
    /**
     * @return string
     */
    public static function getTableName() :string
    {
        return (new self())->getTable();
    }
}
