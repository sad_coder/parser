<?php

namespace App\Traits;

use App\Models\Proxy;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Log;
use Mockery\Exception;


trait ProxyTrait
{

    private static $workingProxy = null;

    /**
     * @param $url
     * @return string
     */
    private function getPageContent($url) :string
    {
        $proxy = $this->getRandomWorkingProxy();
        try {
            $redirectUrl = null;
            $client = new Client([
                'allow_redirects' => true,
            ]);
            $response = $client->get($url, [
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
                    'proxy' => $proxy
                ],

            ]);

            return $response->getBody()->getContents();

        }catch (Exception $e) {
            Proxy::query()->where('proxy',$proxy)->update(['status' => Proxy::PROXY_NOT_WORK]);
            Log::error($e->getMessage());
            self::$workingProxy = null;
            $this->getPageContent($url);
        }
    }

    /**
     * @return string
     */
    private static function getRandomWorkingProxy() :string
    {
        if (!self::$workingProxy){
            self::$workingProxy = Proxy::query()
                ->where('status',Proxy::PROXY_WORK)
                ->inRandomOrder()
                ->value('proxy');
        }
        return self::$workingProxy;
    }

    private function writeTofile() :void
    {
        file_put_contents(storage_path('parsing/output'));
    }
}
