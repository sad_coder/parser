<?php


namespace App\Traits;


use App\Models\Category;

trait ParseTrait
{
    /**
     * @param string $keyword
     * @param int $minPrice
     * @param int $maxPrice
     * @param int $categoryId
     * @return string
     */
    private function getParseUrl( string $keyword, int $minPrice, int $maxPrice, $categoryId) :string
    {
        $categoryUrl = Category::query()->where('id',$categoryId)->value('url');
        return $categoryUrl . "q-{$keyword}/?search%5Bfilter_float_price%3Afrom%5D={$minPrice}&search%5Bfilter_float_price%3Ato%5D={$maxPrice}";
    }
}
