<?php

namespace App\Services\Proxy;

use App\Models\Proxy;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use Illuminate\Support\Arr;

class GetProxies
{
    public $proxySite;
    private $redirectUrl;

    public function __construct()
    {
        $this->proxySite = [
            'getProxyListOfCollectProxyCom',
            'getProxyInProxyListDownload',
            'getProxyListOfFreeProxyNet',
            'getProxySocksInProxyList',
            'getProxyForOnlineProxyRu',
            'getProxyInProxySearcher',
            'freeproxiesListGitDich',
            'scidamProxyListGithub',
            'TheSpeedXGithubProxy',
            'getProxyInProxyPedia',
            'TheSpeedXGithubProxy',
            'h4midFreeProxyList',
            'freeProxyListCom',
            'proxyWebaNetLab',
            'freeSockProxyNet',
            'fate0GithubProxy',
            'a2uGithubProxies',
            'proxyInSuipBiz',
            'coolProxiesCom',
            'clarcetmProxy',
            'rootjazProxy',
            'proxyScrape',
            'usProxyList',
            'vistarProxy',
            'myProxyCom',
            'proxyDaily'
        ];
    }


    public function getProxies()
    {
        $proxies = [];
        foreach ($this->proxySite as $proxySite) {
            try {
                $proxies[]['proxy'] = call_user_func([$this, $proxySite]);
            } catch (Exception $e) {
                $proxySite . ' -> not work';
            }
        }
        $proxies = collect(Arr::flatten($proxies));
        $existsProxies = Proxy::query()->pluck('proxy');
        $proxies = $proxies->whereNotIn('proxy', $existsProxies)->toArray();
        $addToDbProxies = [];
        foreach ($proxies as $k => $proxy) {
            $addToDbProxies[$k]['proxy'] = $proxy;
            $addToDbProxies[$k]['status'] = Proxy::PROXY_NO_CHECK;
        }
        Proxy::query()->insert($addToDbProxies);
    }


    /**
     * @return array
     */
    private function coolProxiesCom(): array
    {
        return $this->getDefaultSimpleProxies('http://coolproxies.com/pl/freeproxylist.php?cc=&grid_id=list1&_search=false&nd=1575075377651&rows=1050&jqgrid_page=1&sidx=free&sord=desc');
    }

    /**
     * @return array
     */
    private function TheSpeedXGithubProxy(): array
    {
        dump('https://github.com/TheSpeedX/PROXY-List/blob/master/http.txt');

        return $this->getDefaultSimpleProxies('https://github.com/TheSpeedX/PROXY-List/blob/master/http.txt');
    }

    /**
     * @return array
     */
    private function a2uGithubProxies(): array
    {
        dump('https://github.com/a2u/free-proxy-list/blob/master/free-proxy-list.txt');

        return $this->getDefaultSimpleProxies('https://github.com/a2u/free-proxy-list/blob/master/free-proxy-list.txt');
    }

    /**
     * @return array
     */
    private function h4midFreeProxyList(): array
    {
        dump('https://github.com/h4mid007/free-proxy-list/blob/master/proxies.txt');

        return $this->getDefaultSimpleProxies('https://github.com/h4mid007/free-proxy-list/blob/master/proxies.txt');
    }

    /**
     * @return array
     */
    private function scidamProxyListGithub(): array
    {
        $content = $this->getProxyPageContent('https://raw.githubusercontent.com/scidam/proxy-list/master/proxy.json');
        $proxyData = [];
        preg_match_all('#"ip": "(\d{1,3}\.\d{1,3}.\d{1,3}.\d{1,3})",\n\s+?"port": "(\d{1,12})",#m', $content, $proxyData);
        $proxies = [];
        foreach ($proxyData[1] as $k => $proxyDatum) {
            $proxies[$k] = $proxyDatum . ':' . $proxyData[2][$k];
        }
        dump('https://raw.githubusercontent.com/scidam/proxy-list/master/proxy.json');

        return $proxies;
    }

    private function proxyListForAll()
    {
        $client = new \GuzzleHttp\Client(['cookies' => true]);
        $r = $client->request('GET', 'https://www.proxylist4all.com/free-proxy-list/free-elite-proxy-list');

        $cookieJar = $client->getConfig('cookies');

        dd($cookieJar->toArray());
    }

    /**
     * @return array
     */
    private function myProxyCom(): array
    {
        $proxies = [
            $this->getDefaultSimpleProxies('https://www.my-proxy.com/free-proxy-list.html'),
            $this->getDefaultSimpleProxies('https://www.my-proxy.com/free-elite-proxy.html'),
            $this->getDefaultSimpleProxies('https://www.my-proxy.com/free-anonymous-proxy.html'),
        ];
        dump('https://www.my-proxy.com');

        return Arr::flatten($proxies);
    }

    /**
     * @return array
     */
    private function freeProxyListCom(): array
    {
        dump('https://free-proxy-list.com/?search=1&page=999&port=&type%5B%5D=http&level%5B%5D=anonymous&level%5B%5D=high-anonymous&up_time=0');
        $contentCountPage = $this->getProxyPageContent('https://free-proxy-list.com/?search=1&page=999&port=&type%5B%5D=http&level%5B%5D=anonymous&level%5B%5D=high-anonymous&up_time=0');
        $pageCount = [];
        $pattern = '#<li class="pager-item pager-current active"><a>(\d+?)</a></li>#m';
        preg_match_all($pattern, $contentCountPage, $pageCount);
        $pageCount = Arr::flatten($pageCount)[1];
        $allProxies = [];
        for ($i = 1; $i < $pageCount; $i++) {
            $url = "https://free-proxy-list.com/?search=1&page={$i}&port=&type%5B%5D=http&level%5B%5D=anonymous&level%5B%5D=high-anonymous&up_time=0";
            $content = $this->getProxyPageContent($url);
            $pattern = '#<a href="\/proxyserver\/port(\d{1,12})\/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\.html"#m';
            $proxies = [];
            preg_match_all($pattern, $content, $proxies);
            foreach ($proxies[2] as $k => $proxy) {
                $portIp[$k] = $proxy . ':' . $proxies[1][$k];
            }
            $allProxies[$i] = $portIp;
        }

        return Arr::flatten($allProxies);
    }

    /**
     * @return array
     */
    private function freeproxiesListGitDich(): array
    {
        dump('https://github.com/a2u/free-proxy-list/blob/master/free-proxy-list.txt');

        return $this->getDefaultSimpleProxies('https://github.com/a2u/free-proxy-list/blob/master/free-proxy-list.txt');
    }

    /**
     * @return array
     */
    private function proxyWebaNetLab(): array
    {
        dump('https://webanetlabs.net/publ/');
        $proxies = [
            $this->getDefaultSimpleProxies('https://webanetlabs.net/publ/24-1-0-1102'),
            $this->getDefaultSimpleProxies('https://webanetlabs.net/publ/9-1-0-669'),
        ];

        return Arr::flatten($proxies);

    }

    /**
     * @return array
     */
    private function clarcetmProxy(): array
    {
        dump('https://raw.githubusercontent.com/clarketm/proxy-list/master/proxy-list.txt');

        return $this->getDefaultSimpleProxies('https://raw.githubusercontent.com/clarketm/proxy-list/master/proxy-list.txt');
    }

    /**
     * @param $url
     * @return array
     */
    private function getDefaultSimpleProxies($url): array
    {
        $content = $this->getProxyPageContent($url);
        $this->writeToFile($content);
        $proxies = [];
        preg_match_all('#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,12}#m', $content, $proxies);
        return Arr::flatten($proxies);

    }

    /**
     * @return array
     */
    private function proxyScrape(): array
    {
        dump('https://api.proxyscrape.com/?request=share&amp;proxytype=http&amp;timeout=10000&amp;country=all&amp;ssl=all&amp;anonymity=elite');
        $url = $this->getRedirectUrl('https://api.proxyscrape.com/?request=share&amp;proxytype=http&amp;timeout=10000&amp;country=all&amp;ssl=all&amp;anonymity=elite');
        return $this->getDefaultSimpleProxies($url);
    }

    /**
     * @return array
     */
    private function vistarProxy(): array
    {
        dump('http://vistar.space/proxy/index?page=999&per-page=100');
        $countPage = [];
        $proxies = [];
        $pageCountUrl = 'http://vistar.space/proxy/index?page=999&per-page=100';
        $countContent = $this->getProxyPageContent($pageCountUrl);
        $countPattern = '#<li><a href="\/proxy\/index\?page=\d+?&amp;per-page=100" data-page="\d+?">(\d+?)<\/a><\/li>#m';
        preg_match_all($countPattern, $countContent, $countPage);
        $pageCount = end($countPage[1]);
        for ($i = 1; $i < $pageCount; $i++) {
            $pageContent = $this->getProxyPageContent("http://vistar.space/proxy/index?page={$i}&per-page=100");
            $onePageProxy = [];
            preg_match_all('#<td>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}<\/td><td>\d{1,12}<\/td>#m', $pageContent, $onePageProxy);
            $proxies[$i] = $this->replaceTd($onePageProxy[0]);
        }

        return Arr::flatten($proxies);
    }

    /**
     * @param array $onePageProxies
     * @return array
     */
    private function replaceTd(array $onePageProxies): array
    {
        $proxies = [];
        foreach ($onePageProxies as $k => $proxy) {
            $proxy = preg_replace('#<\/td><td>#m', ':', $proxy);
            $proxies[$k] = preg_replace('#<\/td>|<td>#m', '', $proxy);
        }

        return $proxies;
    }

    /**
     * @return array
     */
    private function usProxyList(): array
    {
        dump('https://free-proxy-list.net/');
        $response_data = $this->getProxyPageContent('https://free-proxy-list.net/');
        preg_match_all('/<td>([0-9.]{5,}[0-9]{2,})<\/td><td>([0-9]{1,11})<\/td>/m', $response_data, $proxyList);
        $proxyAndPorts = [];
        foreach ($proxyList[1] as $k => $ip) {
            $proxyAndPorts[$k] = $ip . ':' . $proxyList[2][$k];
        }

        return $proxyAndPorts;
    }

    /**
     * @return array
     */
    private function proxyDaily(): array
    {
        dump('https://proxy-daily.com/');

        return $this->getDefaultSimpleProxies('https://proxy-daily.com/');
    }

    /**
     * @param $url
     * @return string
     */
    private function getProxyPageContent($url): string
    {
        $redirectUrl = null;
        $client = new Client([
            'allow_redirects' => true,
        ]);
        $response = $client->get($url, [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
            ],

        ]);

        return $response->getBody()->getContents();
    }


    /**
     * @param $url
     * @return mixed
     */
    private function getRedirectUrl($url): string
    {
        $client = new Client([
            'allow_redirects' => true,
        ]);
        $response = $client->get($url, [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',

            ],
            'on_stats' => function (TransferStats $stats) {
                $this->redirectUrl = $stats->getEffectiveUri() . '/plain';
            },

        ]);

        return $this->redirectUrl;
    }

    /**
     * @return array
     */
    private function getProxyListOfFreeProxyNet(): array
    {
        dump('https://free-proxy-list.net/');
        $response_data = $this->getProxyPageContent('https://free-proxy-list.net/');
        preg_match_all('/<td>([0-9.]{5,}[0-9]{2,})<\/td><td>([0-9]{1,11})<\/td>/m', $response_data, $proxyList);
        $proxyAndPorts = [];
        foreach ($proxyList[1] as $k => $ip) {
            $proxyAndPorts[$k] = $ip . ':' . $proxyList[2][$k];
        }
        return $proxyAndPorts;

    }

    /**
     * @return array
     */
    private function getProxyListOfCollectProxyCom()
    {

        dump('http://www.gatherproxy.com/');
        $response_data = $this->getProxyPageContent('http://www.gatherproxy.com/');
        preg_match_all('#"PROXY_IP":"([0-9.]+)","PROXY_LAST_UPDATE":"[0-9. ]+","PROXY_PORT":"([A-Za-z0-9.]+)"#', $response_data, $rawlist);
        $proxyList = [];
        foreach ($rawlist[1] as $k => $ip) {
            $proxyList[$k] = $ip . ':' . hexdec($rawlist[2][$k]);
        }

        return $proxyList;

    }

    /**
     * @return array
     */
    private function getProxyForOnlineProxyRu(): array
    {
        dump('http://online-proxy.ru/');
        $response_data = $this->getProxyPageContent('http://online-proxy.ru/');
        preg_match_all('/<td>(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})<\/td>\s<td>(\d{1,10})<\/td>/m', $response_data, $proxyList);
        $proxyAndPorts = [];
        foreach ($proxyList[1] as $k => $ip) {
            $proxyAndPorts[$k] = $ip . ':' . $proxyList[2][$k];
        }

        return $proxyAndPorts;
    }

    /**
     * @return mixed
     */
    private function getProxyInProxySearcher(): array
    {
        dump('http://proxysearcher.sourceforge.net/Proxy%20List.php?type=http&filtered=true');
        $response_data = $this->getProxyPageContent('http://proxysearcher.sourceforge.net/Proxy%20List.php?type=http&filtered=true');
        preg_match_all('/<td>(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,12})<\/td>/m', $response_data, $proxyList);

        return $proxyList[1];
    }

    /**
     * @return mixed
     */
    private function getProxySocksInProxyList(): array
    {
        dump('http://proxysearcher.sourceforge.net/Proxy%20List.php?type=socks&filtered=true');
        $response_data = $this->getProxyPageContent('http://proxysearcher.sourceforge.net/Proxy%20List.php?type=socks&filtered=true');
        preg_match_all('/<td>(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,12})<\/td>/m', $response_data, $proxyList);

        return $proxyList[1];
    }

    /**
     * @return mixed
     */
    private function getProxyInProxyPedia(): array
    {
        dump('https://proxypedia.org');
        $response_data = $this->getProxyPageContent('https://proxypedia.org');
        preg_match_all('/<a.+?>(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,12})<\/a>/m', $response_data, $proxyList);

        return $proxyList[1];
    }

    /**
     * @return array
     */
    private function freeSockProxyNet(): array
    {
        dump('https://socks-proxy.net');
        $response_data = $this->getProxyPageContent('https://socks-proxy.net');
        $proxyAndPort = [];
        preg_match_all('/<td>(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})<\/td><td>(\d{1,11})<\/td>/m', $response_data, $proxyList);
        foreach ($proxyList[1] as $k => $proxy) {
            $proxyAndPort[] = $proxy . ':' . $proxyList[2][$k];
        }

        return $proxyAndPort;
    }

    /**
     * @return array
     */
    private function fate0GithubProxy(): array
    {
        dump('https://raw.githubusercontent.com/fate0/proxylist/master/proxy.list');
        $content = $this->getProxyPageContent('https://raw.githubusercontent.com/fate0/proxylist/master/proxy.list');
        $allJson = [];
        preg_match_all('/{.+?}/m', $content, $allJson);
        $idIp = 0;
        $ipPorts = [];
        foreach ($allJson[0] as $item) {
            $item = json_decode($item, true);
            $port = $item['port'];
            $ipAddress = collect($item['export_address'])->filter(function ($ip) use ($port) {
                if (preg_match('/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/m', $ip)) {
                    return $ip . ':' . $port;
                }
            })->unique()->toArray();
            foreach ($ipAddress as $ip) {
                $ipPorts[++$idIp] = $ip;
            }
        }

        return $ipPorts;
    }

    /**
     * @return array
     */
    private function rootjazProxy(): array
    {
        dump('http://rootjazz.com/proxies/proxies.txt');
        $content = $this->getProxyPageContent('http://rootjazz.com/proxies/proxies.txt');
        $proxies = [];
        preg_match_all('/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,11}/m', $content, $proxies);

        return Arr::flatten($proxies);
    }

    /**
     * @return array
     */
    private function getProxyInProxyListDownload(): array
    {
        dump('https://www.proxy-list.download/api/v0/get?l=en&t=http&elite');
        $content = $this->getProxyPageContent('https://www.proxy-list.download/api/v0/get?l=en&t=http&elite');
        $proxyPorts = [];
        $ipPorts = [];
        preg_match_all('/"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})", "PORT": "(\d{1,11})", "ANON": "Elite"/m', $content, $proxyPorts);
        foreach ($proxyPorts[1] as $k => $ip) {
            $ipPorts[] = $ip . ':' . $proxyPorts[2][$k];
        }

        return $ipPorts;
    }

    /**
     * @return mixed
     */
    private function proxyInSuipBiz(): array
    {
        dump('https://suip.biz/?act=proxy1');
        $freeProxies = [];
        $content = $this->getProxyPageContent('https://suip.biz/?act=proxy1');
        preg_match_all('~(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}):\d{1,11}~m', $content, $freeProxies);

        return $freeProxies[0];
    }

    private function premProxyCom()
    {
        dump('https://premproxy.com/list/');
        $paginatorData = [];
        $content = $this->getProxyPageContent('https://premproxy.com/list/');
        preg_match_all('~\<a href="(\d+?)\.htm"\>~m', $content, $paginatorData);
        $pageCount = (int)max($paginatorData[1]);
        $proxyAllpages = collect();
        for ($i = 1; $i <= $pageCount; $i++) {
            $content = $this->getProxyPageContent("https://premproxy.com/list/ip-port/{$i}.htm");
            $proxies = [];
            preg_match_all('~\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,11}~m', $content, $proxies);
            $this->writeTofile($content);
            dd($content);
            $proxyAllpages->push($proxies);
        }
        dd($proxyAllpages->toArray());

        return $paginatorData;
    }

    /**
     * @param $text
     */
    private function writeToFile($text): void
    {
        file_put_contents(storage_path('test2'), $text);
    }
}
