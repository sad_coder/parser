<?php

namespace App\Services\Proxy;

use App\Models\Proxy;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Services\Proxy\Traits\ProxyTrait;

class CheckProxies
{
    use ProxyTrait;

    private static $proxies = null;
    private static $myIp = null;
    private $countWorkProxies;


    /**
     * @param $ch
     * @param $proxy
     */
    private function defaultCurlSettings($ch, $proxy): void
    {
        curl_setopt($ch, CURLOPT_HEADER, true);    // we want headers
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    }

    /**
     * @param $proxy
     * @return string
     */
    public function checkOneProxy($proxy)
    {
        $url = 'https://httpbin.org';
        $loadingtime = time();
        $ch = curl_init($url);
        $this->defaultCurlSettings($ch, $proxy);

        $curlResponse = curl_exec($ch);
        curl_close($ch);
        if ($curlResponse === false || (time() - $loadingtime) > 15) {
            dump($curlResponse);
            dump('proxy -> '.$proxy.' not working');
            Proxy::query()->where('proxy', $proxy)->update(['status' => Proxy::NOT_WORK]);

            return;
        }

        if (strstr($curlResponse, $this->getMyIp())) {
            dump('proxy -> '.$proxy.' not anonymous');
            Proxy::query()->where('proxy', $proxy)->delete();

            return;
        }
        dump('proxy ->'.$proxy.'working');
        Proxy::query()->where('proxy', $proxy)->update(['status' => 'work']);

        return $proxy;
    }

    /**
     * @param $proxyList
     * @return array
     */
    public function AsyncGetWorkingProxies($proxyList): array
    {
        foreach ($proxyList as $k => $proxy) {
            $urls[] = 'http://httpbin.org/ip?i='.$k;
        }

        $multi = curl_multi_init();
        $handles = [];
        foreach ($urls as $k => $url) {
            $ch = curl_init($url);
            $this->defaultCurlSettings($ch, $proxyList[$k]);
            curl_multi_add_handle($multi, $ch);
            $handles[$url]['client'] = $ch;
            $handles[$url]['proxy'] = $proxyList[$k];
        }

        do {
            $mrc = curl_multi_exec($multi, $active);
        } while ($mrc === CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc === CURLM_OK) {
            if (curl_multi_select($multi) == -1) {
                usleep(20);
            }

            do {
                $mrc = curl_multi_exec($multi, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }
        $proxiesCheck = [];
        foreach ($handles as $k => $channel) {
            $html = curl_multi_getcontent($channel['client']);
            $proxiesCheck[$k]['responce'] = $html;
            $proxiesCheck[$k]['proxy'] = $channel['proxy'];
            curl_multi_remove_handle($multi, $channel['client']);
        }
        curl_multi_close($multi);
        $workProxies = [];
        $notWorkProxies = [];
        $proxyKey = 0;
        foreach ($proxiesCheck as $k => $proxy) {
            if (! preg_match('#HTTP/1.1 200 OK#m', $proxy['responce'])
                ||
                preg_match('#'.$this->getMyIp().'#m', $proxy['responce'])
                ||
                ! preg_match('#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}#m', $proxy['responce'])
            ) {
                $notWorkProxies[++$proxyKey] = (string) $proxy['proxy'];

                continue;
            }
            dump($proxy['responce']);
            $workProxies[++$proxyKey]['proxy'] = (string) $proxy['proxy'];
        }
        $this->updateNotWorkProxy($notWorkProxies);

        return $workProxies;
    }

    /**
     * @param array $proxies
     */
    private function updateNotWorkProxy(array $notWorkProxies): void
    {
        Proxy::query()->whereIn('proxy', $notWorkProxies)
            ->update(['status' => Proxy::PROXY_NOT_WORK]);
    }

    public function AsuncUpdateAllWorkProxies(): void
    {
        $proxies = Proxy::query()->pluck('proxy')->toArray();
        $proxyChanks = array_chunk($proxies, Proxy::PROXY_CHUNK_IN_ONE_ASYNC_REQUEST);
        $countWorkingProxies = 0;
        foreach ($proxyChanks as $proxyChank) {
            $workingProxies = $this->AsyncGetWorkingProxies($proxyChank);
            $this->updateOneChankDbWorkProxies($workingProxies);
            $countWorkingProxies += count($workingProxies);
            dump($countWorkingProxies);
        }
        dump($countWorkingProxies);
    }

    private function updateOneChankDbWorkProxies($proxyChank): void
    {
        Proxy::query()->whereIn('proxy', $proxyChank)->update([
            'status' => Proxy::PROXY_WORK,
        ]);
    }

}
