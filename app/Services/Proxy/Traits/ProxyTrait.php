<?php

namespace App\Services\Proxy\Traits;

use App\Models\Proxy;
use Carbon\Carbon;

trait ProxyTrait
{

    /**
     * @return string
     */
    private function getMyIp(): string
    {
        if (self::$myIp === null) {
            self::$myIp = file_get_contents('http://ipecho.net/plain');
        }

        return self::$myIp;
    }




}
