<?php


namespace App\Services;


use App\Models\IgnoredWord;
use App\Models\Keyword;

class KeywordService
{
    /**
     * @param array|null $ignoredWords
     * @return array
     */
    public function getIgnoredWordByRequest(?array $ignoredWords): array
    {
        if ($ignoredWords) {
            $words = [];
            foreach ($ignoredWords as $ignoredWord) {
                $words[]['ignore_word'] = $ignoredWords;
            }
            return $words;
        }

        return [];
    }


    /**
     * @param Keyword $keyword
     * @param array $ignoredWord
     */
    public function attachKeywordAndIgnoredWords(array $keywordIds, array $ignoredWord)
    {
        $attachWords = [];
        $ignoredWordIds = IgnoredWord::query()
            ->whereIn('ignore_word',$ignoredWord)
            ->pluck('id');
        $keywordModels = Keyword::query()->whereIn('id', $keywordIds)->get();
        foreach ($ignoredWordIds as $k => $ignoreWordId) {
           // $attachWords[$k]['keyword_id'] = $ignoreWordId;
            $attachWords[$k]['ignored_word_id'] = $ignoreWordId;
        }
        foreach ($keywordModels as $keywordModel) {
            $keywordModel->ignoredWords()->detach();
            $keywordModel->ignoredWords()->attach($attachWords);
        }


    }


}