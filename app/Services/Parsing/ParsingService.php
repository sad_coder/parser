<?php


namespace App\Services\Parsing;


use App\Models\Advert;
use App\Models\Keyword;
use App\Traits\ParseTrait;
use App\Traits\ProxyTrait;
use GuzzleHttp\Client;
use Log;
use Mockery\Exception;

class ParsingService
{
    use ParseTrait, ProxyTrait;

    private const ALLCONTENTREGEX = '/<td class="offer  ">[\s\S]*?<\/table>/u';
    private const HREFREGEX = '/href="(https.\S+)"/u';
    private const TITLEREGEX = '/<a[\s\S]+?>([\s\S])+?<strong>(.+?)<\/strong>/u';
    private const PRICEREGEX = '/<h3 (.+?)>[\s\S]+<strong>(.+?)<\/strong>/u';
    private const CATEGORYREGEX = '/<small .+>(\s+.+)\s+<\/small>/u';
    private const TIMEREGEX = '/<i data-icon="clock"><\/i>(.+)<\/span>/u';
    private const ALLCOUNTPAGES = '/page=(\d+?)" data-cy="page-link-last">/u';
    private const IMAGEREGEX = '#<img .+?src="(.+?\/image)#mu';


    public function parseAllPages($url, $keyword_id)
    {
        $allProducts = [];
        $pageCount = $this->getCountPage($url);
        dump($pageCount);
        if (!$pageCount) {
            return null;
        }
        $productInformation = [];
        for ($i = 0; $i <= $pageCount; $i++) {
            try {
                $content = $this->getPageContent($url);
                preg_match_all(self::ALLCONTENTREGEX, $content, $allProducts);
                $collectResult = ((collect($allProducts)->flatten()));
                foreach ($collectResult as $key => $res) {
                    $links = [];
                    $title = [];
                    $price = [];
                    $category = [];
                    $time = [];

                    preg_match_all(self::HREFREGEX, $res, $links, PREG_SET_ORDER);
                    preg_match_all(self::TITLEREGEX, $res, $title, PREG_SET_ORDER);
                    preg_match_all(self::PRICEREGEX, $res, $price, PREG_SET_ORDER);
                    preg_match_all(self::CATEGORYREGEX, $res, $category, PREG_SET_ORDER);
                    preg_match_all(self::TIMEREGEX, $res, $time, PREG_SET_ORDER);
                    $productInformation[$i][$key]['title'] = $title[0][array_key_last($title[0])];
                    $productInformation[$i][$key]['price'] = $price[0][array_key_last($price[0])];
                    $productInformation[$i][$key]['send_status'] = Advert::NOT_SEND;
                    $productInformation[$i][$key]['link'] = $links[0][array_key_last($links[0])];
                    $productInformation[$i][$key]['category'] = trim($category[0][array_key_last($category[0])]);
                    $productInformation[$i][$key]['keyword_id'] = $keyword_id;
                    $productInformation[$i][$key]['time'] = trim($time[0][array_key_last($time[0])]);
                    $productInformation[$i][$key]['image_url'] = $this->parseImage($res);
                }
                if (array_key_exists($i, $productInformation)) {
                    $uniqueProducts = $this->insertUniqueAdverts($productInformation[$i]);
                    Advert::query()->insert($uniqueProducts);

                }
            }catch (Exception $e){
                Log::error($e->getMessage());
            }

        }
    }

    private function insertUniqueAdverts($productInformation)
    {
        $existsAdverts = Advert::query()->pluck('link');
        $productInformation = collect($productInformation);
        return $productInformation->whereNotIn('link', $existsAdverts)->toArray();
    }


    private function getCountPage($url)
    {
        $allPages = [];
        $content = $this->getPageContent($url);
        preg_match_all(self::ALLCOUNTPAGES, $content, $allPages, PREG_SET_ORDER);
        dump($url, $allPages);
        if (!empty($allPages)) {
            return $allPages[0][array_key_last($allPages[0])];
        }
        return 1;
    }

    public function getAllKeywords()
    {
        return Keyword::query()->get();
    }

    public function getAllProductLink()
    {
        return Advert::all();
    }

    /**
     * @param string $content
     * @return string|null
     */
    private function parseImage(string $content): ?string
    {
        $images = [];
        preg_match_all(self::IMAGEREGEX, $content, $images, PREG_SET_ORDER);
        if (!empty($images[0])) {
            return $images[0][array_key_last($images[0])];
        }

        return null;

    }


}
