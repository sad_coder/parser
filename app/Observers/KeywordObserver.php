<?php

namespace App\Observers;

use App\Models\Advert;
use App\Models\Keyword;

class KeywordObserver
{
    /**
     * Handle the keyword "created" event.
     *
     * @param \App\Keyword $keyword
     * @return void
     */
    public function created(Keyword $keyword)
    {
        //
    }

    /**
     * Handle the keyword "updated" event.
     *
     * @param \App\Keyword $keyword
     * @return void
     */
    public function updated(Keyword $keyword)
    {
        //
    }


    /**
     * Handle the keyword "deleted" event.
     *
     * @param \App\Keyword $keyword
     * @return void
     */
    public function deleted(Keyword $keyword)
    {
        \Log::info($keyword);
        $keyword->adverts()->update([
            'status' => Advert::DEACTIVATE
        ]);
    }

    /**
     * Handle the keyword "restored" event.
     *
     * @param \App\Keyword $keyword
     * @return void
     */
    public function restored(Keyword $keyword)
    {
        Advert::query()
            ->where('keyword_id', $keyword->id)
            ->update([
                'status' => null
            ]);
    }

    /**
     * Handle the keyword "force deleted" event.
     *
     * @param \App\Keyword $keyword
     * @return void
     */
    public function forceDeleted(Keyword $keyword)
    {
        $keyword->adverts()->delete();
    }
}
