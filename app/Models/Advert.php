<?php

namespace App\Models;

use App\Traits\EntityTableNameTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advert extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];


    use EntityTableNameTrait;

    public const IS_SEND = 1;
    public const NOT_SEND = 0;
    public const IS_DELETED = 'deleted';

    public const DEACTIVATE = 'deactivate';

    public function keywords()
    {
        return $this->belongsTo(Keyword::class, 'keyword_id', 'id')->withTrashed();
    }



}
