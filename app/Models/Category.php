<?php

namespace App\Models;

use App\Traits\EntityTableNameTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use EntityTableNameTrait;
    private $allCategories;

    public const OLX = 'olx.kz';

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function keywords()
    {
        return $this->hasMany(Keyword::class);
    }



}
