<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proxy extends Model
{
    protected $fillable = [
        'proxy',
        'status',
        'work_time',
    ];
    const PROXY_NO_CHECK = 2;
    const PROXY_WORK = 1;
    const PROXY_NOT_WORK = 0;
    const PROXY_CHUNK_IN_ONE_ASYNC_REQUEST = 50;
}
