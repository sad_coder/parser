<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Keyword extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'keyword',
        'price_min',
        'price_max',
        'status',
        'category_id',
        'search_url',
        'importance',
    ];
    public const IS_SEND = 'send';
    public const NOT_SEND = 'not_send';
    public const IS_DELETE = 'deleted';
    public const ACTIVE = 'active';
    public const NO_ACTIVE = 'no_active';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function adverts()
    {
        return $this->hasMany(Advert::class, 'keyword_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    /**
     * @return array
     */
    public static function getStatusesArray(): array
    {
        return [
            self::IS_SEND,
            self::NO_ACTIVE,
            self::NOT_SEND,
            self::IS_DELETE,
        ];
    }

    public function ignoredWords()
    {
        return $this->belongsToMany(IgnoredWord::class);
    }

    public function getKeywordWithPriceAttribute($value)
    {
        return $this->keyword . ' -> ( ' . $this->price_min . ' - ' . $this->price_max . ')';
    }

    public function getAdvertsCountAttribute($value)
    {
        return $this->adverts()->count();
    }


}
