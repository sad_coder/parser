<?php

namespace App\Console;

use App\Console\Commands\Parsing\MainParser;
use App\Console\Commands\Parsing\Market\ParseMarketCategories;
use App\Console\Commands\ProductParse;
use App\Console\Commands\Proxy\GetProxies;
use Illuminate\Console\Scheduling\Schedule;
use App\Console\Commands\Proxy\CheckProxies;
use App\Console\Commands\Parsing\ParseCategories;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ProductParse::class,
        GetProxies::class,
        CheckProxies::class,
        MainParser::class,
        ParseMarketCategories::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
