<?php

namespace App\Console\Commands;

use App\Models\Advert;
use Illuminate\Console\Command;
use App\Http\Controllers\Parsing;

class ProductParse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = microtime(true);
        $parser = new Parsing();
        $allProductLink = $parser->getAllProductLink()->toArray();
        $keywords = $parser->getAllKeywords();
        foreach ($keywords as $keyword) {
            $parse_url = $parser->getParseUrl($keyword->keyword, $keyword->price_min, $keyword->price_max);
            $parse_product = collect($parser->parseAllPages($parse_url))->all();
            foreach ($parse_product as $productOnePage) {
                $productOnePage = collect($productOnePage)->diff($allProductLink);
                Advert::insert($productOnePage);
            }
        }
        $end = microtime(true) - $start;
        $this->info('на парсинг ушло'.$end.'секунд');
    }
}
