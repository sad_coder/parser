<?php

namespace App\Console\Commands\Proxy;

use Illuminate\Console\Command;
use App\Services\Proxy\CheckProxies as ProxyChecker;
use Log;
use Mockery\Exception;

class CheckProxies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'proxy:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for (;;){
            try {
                $proxyChecker = new ProxyChecker();
                $proxyChecker->AsuncUpdateAllWorkProxies();
            }catch (Exception $e){
                dd($e->getMessage());
                Log::error($e->getMessage());
            }
        }
    }
}
