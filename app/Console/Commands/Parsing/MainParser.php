<?php

namespace App\Console\Commands\Parsing;

use Log;
use DB;
use Telegram\Bot\Api;
use Mockery\Exception;
use App\Models\Advert;
use App\Models\Keyword;
use Illuminate\Console\Command;
use App\Services\Parsing\ParsingService;
use Illuminate\Database\Eloquent\Collection;

class MainParser extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'start:parsing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private $telegram;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->telegram = new Api('1023746736:AAEW0L88hwlA8pSGAWOVXsvT6_4rlvewRF0');
        //'chat_id' => '921722273', //Petr ID
        $parsingService = new ParsingService();
        for (; ;) {
            $allKeywords = $this->getKeywordsForParsing();
            $countAllCeywords = count($allKeywords);
            for ($i = 0; $i < $countAllCeywords; $i++) {
                try {
                    $parsingService->parseAllPages($allKeywords[$i]['search_url'], $allKeywords[$i]['id']);
                } catch (Exception $e) {
                    --$i;
                    dd($e->getMessage());
                    Log::error($e->getMessage());
                }
            }

            $allAdvertsForSend = $this->getAdvertsForSendMessage();
            $allCountAdverts = count($allAdvertsForSend);
            for ($i = 0; $i < $allCountAdverts; $i++) {
                try {
                    $this->senMessageInTelegramm($allAdvertsForSend[$i]);
                    usleep(5000);
                } catch (Exception $e) {
                    dd($e->getMessage());
                    Log::info($e->getMessage());
                    --$i;
                }
            }
            sleep(900);
        }
    }


    /**
     * @param $notSendAdverts
     */
    private function senMessageInTelegramm(?array $notSendAdverts): void
    {
        if ($notSendAdverts) {
            $messageText = $this->getMessageText($notSendAdverts);
            $this->telegram->sendMessage([
                'chat_id' => '609622313',
                'text' => $messageText
            ]);
            dump($messageText);
            DB::table(Advert::getTableName())
                ->where('id', $notSendAdverts['id'])
                ->update(['send_status' => Advert::IS_SEND]);
        }


    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getAdvertsForSendMessage(): ?array
    {
        return Advert::query()
            ->where('send_status', Advert::NOT_SEND)
            ->get([
                'id',
                'title',
                'time',
                'price',
                'link',
                'send_status',
            ])
            ->toArray();
    }


    /**
     * @param $sendAdvert
     * @return string
     */
    private function getMessageText($sendAdvert): string
    {
        return $sendAdvert['title'] . ' Стоимость->' . $sendAdvert['price'] . ' Добавлено ' . $sendAdvert['time'] . ' ссылка ' . $sendAdvert['link'];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|Collection
     */
    private function getKeywordsForParsing(): ?array
    {
        return Keyword::query()
            ->orderBy('importance')
            ->where('status', Keyword::ACTIVE)
            ->get(['id', 'search_url'])
            ->toArray();
    }


}
