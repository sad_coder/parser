<?php

namespace App\Console\Commands\Parsing\Market;

use App\Traits\ProxyTrait;
use Illuminate\Console\Command;

class ParseMarketCategories extends Command
{
    use ProxyTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parseMarket:categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $getMainCategoriesRegex = '<li class="main-seo-site-links__item"><a href="https:\/\/market.kz\/.+?\/">.+?<\/a><\/li>';

        $data = $this->getPageContent('https://market.kz');
        file_put_contents(storage_path('test2'),$data);
        dd(1);
    }
}
