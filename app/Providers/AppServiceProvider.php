<?php

namespace App\Providers;

use App\Models\Keyword;
use App\Observers\KeywordObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Keyword::observe(KeywordObserver::class);
    }
}
