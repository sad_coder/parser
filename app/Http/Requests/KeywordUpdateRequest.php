<?php

namespace App\Http\Requests;

use App\Models\Category;
use App\Models\Keyword;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class KeywordUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'keyword' => 'required|string',
            'status' => 'in:' . implode(',',Keyword::getStatusesArray()),
            'price_max' => 'required|int',
            'price_min' =>'required|int|max:price_max',
            'category_id' =>'required|int|exists:'.Category::getTableName() .',id',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'keyword.required' => 'keyword_required',
            'price_max.required' => 'price_max_required',
            'price_min.required' => 'price_min_required',
            'category_id.required' => 'category_ids_required',
        ];
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(['errors' => $errors,
        ], JsonResponse::HTTP_BAD_REQUEST));
    }


}
