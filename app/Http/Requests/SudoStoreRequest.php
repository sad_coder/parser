<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SudoStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4',
            'email' => 'required',
            'password' => 'required|min:8',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name field has should required',
            'name.min' => 'Name field has should a 4 min character',
            'email.required' => 'Email field has should required',
            'password.required' => 'Password field has should required',
            'password.min' => 'Name field has should a 8 min character',
        ];
    }
}
