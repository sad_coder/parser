<?php

namespace App\Http\Controllers;

use App\Models\Advert;
use Illuminate\Http\Request;


class AdvertController extends Controller
{
    public function index()
    {
        $adverts = Advert::query()
            ->where('status', null)
            ->with('keywords')
            ->paginate(20);
        return view('adverts', compact('adverts'));
    }


    /**
     * @param $request
     */
    public function delete(Request $request)
    {

        Advert::query()
            ->where('id', $request->get('delete_id'))
            ->delete();
        return response()->json('success');
    }

}
