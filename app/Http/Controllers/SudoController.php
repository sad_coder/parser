<?php

namespace App\Http\Controllers;

use App\Http\Requests\SudoStoreRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SudoController extends Controller
{
    public function create()
    {
        $user = Auth::user();

        if ($user->can('create', User::class)) {
            return view('auth.register');
        }
        return redirect('/');
    }

    public function store(SudoStoreRequest $request)
    {
        User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'role' => User::MODERATOR_ROLE,
            'keywords_count' => $request->get('keywords_count')
        ]);
        return response()->json(['messages' => 'success'], 200);
    }
}
