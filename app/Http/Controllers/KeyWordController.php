<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateKeywordsRequest;
use App\Http\Requests\KeywordUpdateRequest;
use App\Models\Advert;
use App\Models\Category;
use App\Models\IgnoredWord;
use App\Models\Keyword;
use App\Services\KeywordService;
use App\Traits\ParseTrait;
use Illuminate\Http\Request;

class KeyWordController
{
    use ParseTrait;

    private $keywordService;

    public function __construct()
    {
        $this->keywordService = new KeywordService();
    }

    public function view()
    {
        $categories = Category::query()->get();
        return view('form', compact('categories'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $keyWords = Keyword::query()
            ->withTrashed()->with('categories')
            ->orderBy('importance', 'desc')
            ->orderBy('deleted_at', 'asc')
            ->paginate(15);

        return view('key_words', compact('keyWords'));
    }

    /**
     * @param CreateKeywordsRequest $request
     */
    public function createKeywords(CreateKeywordsRequest $request)
    {
        \Log::info($request->all());
        $keywordService = new KeywordService();
        $ignoredWords = $keywordService->getIgnoredWordByRequest($request->get('ignoredWords'));

        $keywordIds = [];
        foreach ($request->get('category_ids') as $categoryId) {
            $fields = [
                'keyword' => $request->get('keyword'),
                'search_url' => $this->getParseUrl(
                    $request->get('keyword'),
                    $request->get('price_min'),
                    $request->get('price_max'),
                    $categoryId
                ),
                'importance' => $request->get('importance'),
                'status' => Keyword::ACTIVE,
                'price_min' => $request->get('price_min'),
                'price_max' => $request->get('price_max'),
                'category_id' => $categoryId,
            ];
            $keywordIds[] = Keyword::query()->insertGetId($fields);
        }
        $keywordService->attachKeywordAndIgnoredWords($keywordIds, $ignoredWords);
        return response()->json('success');
    }


    /**
     * @param KeywordUpdateRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function formUpdateKeyword(Request $request)
    {
        $keyword = Keyword::query()
            ->with('categories','ignoredWords')
            ->where('id', $request->get('id'))
            ->first();

        $categories = Category::query()->get();
        return view('update_keyword', compact('keyword', 'categories'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        Keyword::query()
            ->where('id', $request->get('delete_id'))
            ->forceDelete();
        return response()->json('success');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activateDeactivate(Request $request)
    {
        if ($request->get('type') === 'deactivate') {
            Keyword::query()->where('id', $request->get('deactivate_id'))->delete();
            Advert::query()->where('keyword_id', $request->get('deactivate_id'))->update([
                'status' => Advert::DEACTIVATE
            ]);
        }
        if ($request->get('type') === 'activate') {
            Keyword::query()->where('id', $request->get('activate_id'))->restore();
            Advert::query()->where('keyword_id', $request->get('activate_id'))->update([
                'status' => null
            ]);
        }
        return response()->json('success');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAdvertsByKeywordId(Request $request)
    {
        $keyword = Keyword::query()->withTrashed()->find($request->get('keyword_id'));
        $keywordsAdverts = $keyword->adverts()
            ->paginate(20);
        return view('adverts_by_keyword', compact('keywordsAdverts'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxIgnoredKeywordSearch(Request $request)
    {
        $ignoredKeyWord = $request->get('ignored_keyword');
        $words = IgnoredWord::query()
            ->where('ignore_word', 'LIKE', '%' . $ignoredKeyWord . '%')
            ->limit(16)
            ->pluck('ignore_word')
            ->toArray();
        return response()->json($words);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxAddIgnoredWord(Request $request) :object
    {
        $checkExistsIgnoredWord = IgnoredWord::query()
            ->where('ignore_word',$request->get('ignored_word'))
            ->first();
        if (!$checkExistsIgnoredWord) {
            IgnoredWord::query()->insert(['ignore_word'=>$request->get('ignored_word')]);
            return response()->json('add_keyword');
        }
        return response()->json('keyword_exists');
    }


    public function update(KeywordUpdateRequest $request)
    {

        $this->keywordService->attachKeywordAndIgnoredWords([$request->keyword],$request->ignoredWords);
        Keyword::query()->where('id',$request->get('keywordId'))->update([
           'keyword' => $request->get('keyword'),
           'price_min' => $request->get('price_min'),
           'price_max' => $request->get('price_max'),
           'importance' => $request->get('importance'),
           'category_id' => $request->get('category_id'),
        ]);

        return response()->json('success');
    }

}
