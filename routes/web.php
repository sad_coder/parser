<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('parsing', 'Parsing@parsing');

//key words

Route::group(['middleware'=>'auth:web'],function (){
    Route::get('form_keywords', 'KeyWordController@view')->name('form_keywords');
    Route::post('add_keywords', 'KeyWordController@createKeywords')->name('add_keywords');
    Route::get('form_update_keywords','KeyWordController@formUpdateKeyword')->name('form_update_keyword');
    Route::post('keyword_update','KeyWordController@keywordUpdate')->name('keyword_update');
    Route::get('/', 'KeyWordController@index')->name('keyword.all');
    Route::post('delete_keywords','KeyWordController@delete')->name('delete_keyword');
    Route::post('activate_deactivate_keyword','KeyWordController@activateDeactivate')->name('activate_deactivate_keyword');
    Route::get('show_adverts_by_keyword_id','KeyWordController@showAdvertsByKeywordId')->name('show_adverts_by_keyword_id');
    Route::post('update_keyword','KeyWordController@update')->name('update_keyword');
    //ignored keywords
    Route::post('get_ajax_ignored_keyword','KeyWordController@ajaxIgnoredKeywordSearch')->name('ajax_ignored_word');
    Route::post('add_ajax_ignored_word','KeyWordController@ajaxAddIgnoredWord')->name('ajax_add_ignored_word');
//adverts
    Route::get('adverts', 'AdvertController@index')->name('advert.all');
    Route::post('delete_adverts','AdvertController@delete')->name('advert.delete');

});

Route::get('/sudo/users/add', 'SudoController@create');
Route::post('/sudo/users/add', 'SudoController@store');


Auth::routes(['register' => false]);

