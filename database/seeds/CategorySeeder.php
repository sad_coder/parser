<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 0;
        $categories = [];
        foreach ($this->getCategoriesUrl() as $name => $categoryUrl) {
            $categories[++$i]['url'] = $categoryUrl;
            $categories[$i]['provider'] = Category::OLX;
            $categories[$i]['name'] = $name;
        }
        Category::query()->insert($categories);
    }

    /**
     * @return array
     */
    private function getCategoriesUrl(): array
    {
        return [
            'Дом сад ->прочие товары для дома' => 'https://www.olx.kz/dom-i-sad/prochie-tovary-dlya-doma/alma-ata/',
            'Электроника ->прочая электроника' => 'https://www.olx.kz/elektronika/prochaja-electronika/alma-ata/',
            'Электроника ->техника для дома' => 'https://www.olx.kz/elektronika/tehnika-dlya-doma/alma-ata/',
            'Электроника ->климатическое оборудование' => 'https://www.olx.kz/elektronika/klimaticheskoe-oborudovanie/alma-ata/',
            'Электроника ->аксуссуары и комплектующие' => 'https://www.olx.kz/elektronika/aksessuary-i-komplektuyuschie/alma-ata/',
            'Дом сад ->инструменты' => 'https://www.olx.kz/elektronika/prochaja-electronika/alma-ata/',
            'Дом сад ->инструменты бензоинструмент' => 'https://www.olx.kz/dom-i-sad/instrumenty/benzoinstrument/alma-ata/',
            'Дом сад ->инструменты электроинструмент' => 'https://www.olx.kz/dom-i-sad/instrumenty/elektroinstrument/alma-ata/',
            'Дом сад ->инструменты пневмоинструмент' => 'https://www.olx.kz/dom-i-sad/instrumenty/pnevmoinstrument/alma-ata/',
            'Дом сад ->инструменты прочий инструмент' => 'https://www.olx.kz/dom-i-sad/instrumenty/prochiy-instrument/alma-ata/',
            'Дом сад ->инструменты ручной инструмент' => 'https://www.olx.kz/dom-i-sad/instrumenty/prochiy-instrument/alma-ata/',
            'Дом сад ->строительство и ремонт' => 'https://www.olx.kz/dom-i-sad/stroitelstvo-remont/alma-ata/',
            'Дом сад ->строительство и ремонт электрика' => 'https://www.olx.kz/dom-i-sad/stroitelstvo-remont/elektrika/alma-ata/',
            'Дом сад ->строительство и ремонт сантехника' => 'https://www.olx.kz/dom-i-sad/stroitelstvo-remont/santehnika/alma-ata/',
        ];
    }





}



