<?php

use App\Models\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactoringCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Category::getTableName(), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('provider');
            $table->string('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(\App\Category::getTableName(),function (Blueprint $table){
           $table->dropColumn('provider');
           $table->dropColumn('name');
           $table->dropColumn('id');
        });
    }
}
