<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIgnoredKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ignored_keywords', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ignore_word');
            $table->timestamps();
        });
        Schema::create('keyword_ignored_word', function (Blueprint $table) {
            $table->unsignedBigInteger('keyword_id');
            $table->unsignedBigInteger('ignored_word_id');

        });

    }

    /**
     * Reverse the migrations.
     
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ignored_keywords');
        Schema::dropIfExists('keyword_ignored_word');
    }
}
