<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactoringProxiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proxies', function (Blueprint $table) {
            $table->dropColumn('support_in_google');
            $table->dropColumn('work_count');
            $table->dropColumn('check_count');
            $table->timestamp('work_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adverts', function (Blueprint $table) {
            $table->string('support_in_google');
            $table->integer('work_count');
            $table->integer('check_count');
            $table->dropColumn('work_time');
        });
    }
}
