@include('layouts.main')

<style>
    .main_table {
        margin-top: 3%;
        width: 100%;
        display: flex;
    }

    .main_table > .table {
        margin-left: 10%;
        margin-right: 10%;
    }

    .imager > img {
        width: 200px;
        height: 150px;
    }

</style>


<div class="main_table">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Заголовок</th>
            <th scope="col">Действия</th>
            <th scope="col">Цена</th>
            <th scope="col">Картинка</th>
            <th scope="col">Категория</th>
            <th scope="col">Ключевое слово</th>
        </tr>
        </thead>
        <tbody>
        @foreach($adverts as $advert)

            <tr class="my-Tr">
                <th class="my-th" scope="row">{{$advert->id}}</th>
                <td class="my-td"><a href="{{$advert->link}}"> {{$advert->title}}</a></td>
                <td class="my-td"><button type="button" data-id = "{{$advert->id}}" class="mybtn btn btn-danger">Удалить</button></td>
                <td class="my-td">{{$advert->price}}</td>
                <td class="my-td">
                    <a href="{{$advert->link}}">
                        <div class="imager">
                            <img src="{{$advert->image_url}}" alt="{{$advert->title}}">
                        </div>
                    </a></td>
                <td class="my-td">{{$advert->category}}</td>
                <td class="my-td"><a href="{{route('show_adverts_by_keyword_id',['keyword_id' => $advert->keywords->id])}}">{{$advert->keywords->keywordWithPrice}}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
<div class="my-row">
    <div class="col12 text-center">
        {{$adverts->links()}}
    </div>
</div>

<script>
    $("button.mybtn.btn.btn-danger").click(function(e){
        let delete_id = $(this).attr('data-id');
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/delete_adverts',
            data: {
                delete_id: delete_id,
            },

            success: function (response) {
                console.log(response);
            },
        });
        $(this).closest(".my-Tr").remove();
        return false;
    });
</script>



