<style>
    .send_success {
        width: 100%;
        text-align: center;
        background-color: green;
        color: white;
        font-size: 24px;
        text-shadow: 0px 2px 0px black;
        border-radius: 20px;
        margin-bottom: 2%;
        transition: .4s;
    }

    .send_error {
        width: 100%;
        text-align: center;
    }


    form {
        display: flex;
        flex-direction: column;
        width: 50%;
        justify-content: center;
        padding: 25px;
    }

    input[type="text"] {
        text-align: center;
        font-size: 20px;
        font-weight: 800;
    }

    .main {
        justify-content: center;
        display: flex;
        margin-top: 10%;
    }

    span.govno {
        text-align: center;
        font-weight: 600;
    }


    input[type="submit"] {
        width: 20%;
        margin-left: 40%;
        padding: 10px;
        border-radius: 10px;
    }

    input.data-texter {
        outline: none;
    }

    input.data-texter:focus {
        box-shadow: 0px 0px 3px #007bff;
    }


    .main {
        flex-wrap: wrap;
    }

    .send_error {
        background-color: #f75050;
        color: white;
        font-size: 24px;
        text-shadow: 0px 2px 0px black;
        border-radius: 20px;
        margin-bottom: 1%;
        padding: 4px;
    }

    .send_success {
        padding: 4px;
    }

    .send_error {
        display: none;
    }

    .send_success {
        display: none;
    }

    button#clack {
        width: 40%;
        margin-left: 30%;
        padding: 13px;
        font-size: 19px;
        font-weight: 900;
        text-shadow: -1px 2px 0px black;
    }

    #errors {
        margin-top: 5%;
        text-align: center;
        margin-left: 25%;
        margin-right: 25%;
        margin-bottom: -9%;
        border-radius: 15px;
        color: #fff;
        background: #f75050;
        font-size: 18px;
        text-shadow: -1px 1px 0px black;
    }

    .send_success {
        margin-left: 25%;
        margin-right: 25%;
        border-radius: 5px;
    }

    form#govnoForm {
        margin-top: 4%;
    }


    .form-group.my-categories {
        position: absolute;
        margin-left: 49%;
        height: 71%;
        text-align: center;
        font-size: 18px;
        margin-top: 4.2%;
    }

    select.form-control[multiple], select.form-control[size] {
        height: 80% !important;
    }

    .box {
        text-align: center;
    }

    h2 {
        padding: 30px 0;
    }


    label:hover {
        background-color: #eee;
    }

    input {
        margin: 0 10px;
    }

    label.radiobutton_label {
        padding: 17px;
        border-radius: 10px;
    }

    label.radiobutton_label:nth-child(1) {
        background: green;
        color: #fff;
    }

    label.radiobutton_label:nth-child(2) {
        background: #8ca928;
    }

    label.radiobutton_label {
        color: #fff;
        box-shadow: -2px 1px 0px 0px #0000002e;
        text-shadow: 0px 2px 0px black;
    }

    label.radiobutton_label:nth-child(3) {
        background: #dcbd0e;
    }

    label.radiobutton_label:nth-child(4) {
        background: #de6c01;
    }

    label.radiobutton_label:nth-child(5) {
        background: #de4301;
    }

    .deleteIgnoredIcon {
        width: 19px;
        position: absolute;
        top: -4%;
        right: -3%;
        transition: .3s;
    }

    .ignore_keywords:hover > svg {
        transform: scale(1);
    }

    .ignore_keywords {
        position: relative;
    }

    button.remove {
        width: 25px;
        background: #ff000000;
        border: none;
        position: absolute;
        top: -8%;
        right: 0%;
        transform: scale(0);
        transition: .4s;
    }

    .ignore_keywords:hover > button.remove {
        transform: scale(1);
    }


</style>

@include('layouts.main')

<body>

<div id="errors"></div>
<div class="main">

    <div class="send_success">Успешно добавлено</div>

    <form id="govnoForm" action="{{route('add_keywords')}}" onkeydown="return event.key != 'Enter'" ; method="POST">
        @csrf
        <input type="text" autocomplete="off" placeholder="Ключевое слово" name="keyword" class="data-texter"><br>
        <input type="text" autocomplete="off" placeholder="Минимальная цена" name="price_min" class="data-texter"><br>
        <input type="text" autocomplete="off" placeholder="Максимальная цена" name="price_max" class="data-texter"><br>
        <div id="ignoretags">

        </div>
        <input type="text" autocomplete="off" onkeydown="isKeyPressed(event)" placeholder="Игнорируемые ключевые слова"
               name="price_max" id="ignoreTag" class="data-texter">
        <select onchange="getSelectValue()" onkeydown="cleanSelectValue(event)" multiple class="form-control"
                name="selectedKeyWord" id="selectedKeywords">
        </select>

        <br>

        <span class="govno">Уровень важности</span><br>
        <div class="box">
            <label class="radiobutton_label"><input type="radio" class="radiobutton" name="importance" value="1"
                                                    tabindex="0">0</label>
            <label class="radiobutton_label"><input type="radio" class="radiobutton" name="importance" value="2"
                                                    tabindex="1">1</label>
            <label class="radiobutton_label"><input type="radio" class="radiobutton" name="importance" value="3"
                                                    tabindex="2">2</label>
            <label class="radiobutton_label"><input type="radio" class="radiobutton" name="importance" value="4"
                                                    tabindex="3">3</label>
            <label class="radiobutton_label"><input type="radio" class="radiobutton" name="importance" checked value="5"
                                                    tabindex="4">4</label>
        </div>
        <div class="form-group my-categories">
            <label for="exampleFormControlSelect1">Выбор Категории</label>
            <select multiple="multiple" name="category_id" class="form-control" id="mySelectForm">
                @foreach($categories as $k => $category)
                    @if ($k === 1)
                        <option selected="selected" value="{{$category->id}}">{{$category->name}}</option>
                        @continue
                    @endif
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>
        {{--        <input type="submit" id="clack" class="btn btn-success" value="Клац-клац" id="1">--}}
        <button type="submit" id="clack" class="btn btn-success">Клац-клац добавить</button>
    </form>
</div>
<script src="{{asset('js/script.js')}}"></script>


<script type="text/javascript">
    let closeSvg = '<button type="button" class="remove"><svg class="deleteIgnoredIcon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">\n' +
        '<circle style="fill:#D75A4A;" cx="25" cy="25" r="25"/>\n' +
        '    <polyline style="fill:none;stroke:#FFFFFF;stroke-width:7;stroke-linecap:round;stroke-miterlimit:10;" points="16,34 25,25 34,16   "/>\n' +
        '    <polyline style="fill:none;stroke:#FFFFFF;stroke-width:7;stroke-linecap:round;stroke-miterlimit:10;" points="16,16 25,25 34,34   "/>\n' +
        '</svg></button>';
    let ignoredKeywords = [];
    let CountKeyword = 0;
    let selectedValues = [];
    let IgnoredWordSelect = document.getElementById("selectedKeywords");

    function isKeyPressed(event) {
        let ignoredKeyword = $('#ignoreTag').val();
        $.ajax({
            type: 'POST',
            url: '/get_ajax_ignored_keyword',
            data: {
                ignored_keyword: ignoredKeyword,
            },


            success: function (response) {
                let removedOldSearchElements = document.getElementsByClassName("KeywordOption");
                while (removedOldSearchElements[0]) {
                    removedOldSearchElements[0].parentNode.removeChild(removedOldSearchElements[0]);
                }
                response.forEach(function (ignoredWord) {
                    let option = document.createElement("option");
                    option.text = ignoredWord;
                    option.value = ignoredWord;
                    option.className = "KeywordOption";
                    IgnoredWordSelect.setAttribute('size', response.length);
                    IgnoredWordSelect.add(option);
                })
            },
        });

        if (event.keyCode == 13) {
            let regex = new RegExp("^\s*$");
            let input = $('#ignoreTag').val();
            let inputCheck = regex.test(input);
            if (!inputCheck) {
                let ignoreValue = $('#ignoreTag').val();
                console.log(ignoreValue);
                if(!ignoredKeywords.includes(ignoreValue)){

                    $('#ignoretags').append('<div data-keyword-id=' + CountKeyword + ' class="ignore_keywords">' + ignoreValue + closeSvg + '</div>');
                    ignoredKeywords.push(ignoreValue);
                    CountKeyword++;

                    let ignoredWord = ignoreValue;
                    $.ajax({
                        type: 'POST',
                        url: '/add_ajax_ignored_word',
                        dataType: 'JSON',
                        data: {
                            ignored_word: ignoredWord,
                        }
                    });
                }
                $('input#ignoreTag').val('');

            }
        }
    }

    function getSelectValue() {
        let selectElement = document.getElementById('selectedKeywords');
        selectedValues = Array.from(selectElement.selectedOptions)
            .map(option => option.value);


    }

    function cleanSelectValue(event) {

        if (event.keyCode == 13) {
            selectedValues.forEach(function (ignoreValue) {
                    if (!ignoredKeywords.includes(ignoreValue)) {
                        ignoredKeywords.push(ignoreValue);
                        $('#ignoretags').append('<div data-keyword-id=' + CountKeyword + ' class="ignore_keywords">' + ignoreValue + closeSvg + '</div>');
                    }
                }
            );
        }
    }


    $(document).on('click', "button.remove", function (e) {
        e.preventDefault();
        let deletedKeywordValue = this.parentElement.textContent.replace(/\s+/g, " ").trim();
        ignoredKeywords = ignoredKeywords.filter(e => e !== deletedKeywordValue);
        $(this).closest(".ignore_keywords").remove();
        return false;
    });


    $.ajaxSetup({

        headers: {
            'X-CSRF-TOKEN': $("input[name=_token]").val()
        }

    });

    $("#clack").click(function (e) {
        $('#errors').text('');
        $('.send_success').css("display", "none");


        e.preventDefault();
        let keyword = $("input[name=keyword]").val();
        let price_min = $("input[name=price_min]").val();
        let price_max = $("input[name=price_max]").val();
        let importance = $("input[name='importance']:checked").val();
        let category_ids = $("#mySelectForm").val();
        let ignoredWords = $('.ignore_keywords').map(function () {
            return $.trim($(this).text());
        }).get();

        var resultArray = [];


        console.log(ignoredWords);
        let errorsHanglig = {
            'keyword_required': 'ключевое слово обязательный параметр',
            'price_min_required': 'миниамальная цена обязательный параметр',
            'price_max_required': 'максимальная цена обязательный параметр',
            'category_id_required': 'категория обязательный параметр',
        };
        $.ajax({
            type: 'POST',
            url: '/add_keywords',
            data: {
                keyword: keyword,
                price_min: price_min,
                price_max: price_max,
                importance: importance,
                category_ids: category_ids,
                ignoredWords: ignoredWords,
            },


            success: function (response) {
                $('.send_success').css("display", "block");
                $('.data-texter').val('');
            },
            error: function (errormessage) {
                let errors = JSON.parse(errormessage.responseText);
                $.each(errors.errors, function (index, value) {
                    $('#errors').append(errorsHanglig[value] + '<br>');
                })
            }

        });
    });

</script>


<script>

    $(function () {
        $('.radiobutton').radioTabbable();
    });

    $.fn.radioTabbable = function () {
        var groups = [];

        // group the inputs by name
        $(this).each(function () {
            var el = this;
            var thisGroup = groups[el.name] = (groups[el.name] || []);
            thisGroup.push(el);
        });

        $(this).on('keydown', function (e) {
            setTimeout(function () {
                var el = e.target;
                var thisGroup = groups[el.name] = (groups[el.name] || []);
                var indexOfTarget = thisGroup.indexOf(e.target);

                if (e.keyCode === 9) {
                    if (indexOfTarget < (thisGroup.length - 1) && !(e.shiftKey)) {
                        thisGroup[indexOfTarget + 1].focus();
                    } else if (indexOfTarget > 0 && e.shiftKey) {
                        thisGroup[indexOfTarget - 1].focus();
                    }
                }
            });
        });
    };


</script>


<script>
    $('option').mousedown(function (e) {
        e.preventDefault();
        $(this).prop('selected', !$(this).prop('selected'));
        return false;
    });
</script>


</body>
