<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <meta charset="utf-8">
</head>

<style>

    label.radiobutton_label {
        cursor: pointer;
    }
    html{
        overflow-x:hidden
    }
    .flex-container {
        display: flex;
        width: 100%;
        margin-left: 15%;
        margin-right: 15%;
        margin-top: 3%;
        /* border-bottom: 1px solid #e0e4e7; */
    }


    .menu-item {
        width: 13%;
        margin-left: 1%;
        margin-right: 1%;
        color: #fff;
        font-weight: 900;
        font-size: 15px;
        text-shadow: -1px 1px 0px black;
        padding-bottom: 15px;
        padding-top: 15px;
        text-align: center;
        font-family: PT Sans, Roboto, Helvetica Neue, Arial, sans-serif;
        background-color: #6fcfb0;
        border-radius: 6px;
        transition: .4s;
        cursor: pointer;
    }

    .menu-item:hover {
        background-color: #099a6b;
    }

    a.menu_text {
        text-decoration: none !important;
        color: #fff !important;
    }

    .my-row {
        margin-left: 40%;
    }

    button#clack {
        background-image: linear-gradient(to top, #0ba360 0%, #3cba92 100%);
        transition: .4s;
    }

    button#clack:hover {
        box-shadow: -3px 5px 0px #6fcfb0;
        border: 0px;
        transform: scale(1.05);
    }

    form#govnoForm {
        background: #f7f8fed1;
    }

    .ignore_keywords {
        background: #63e6bc;
        padding: 10px;
        color: white;
        text-shadow: 0px 1px 0px black;
        font-weight: 900;
        border-radius: 10px;
        margin-left: 1.5%;
        margin-top: 1%;
    }

    div#ignoretags {
        display: flex;
        flex-wrap: wrap;
        margin-bottom: 3%;
    }


</style>

<div class="flex-container">
    <a href="{{route('keyword.all')}}" class="menu_text">
        <div class="menu-item"><a href="{{route('keyword.all')}}" class="menu_text">Ключевые слова</a></div>
    </a>
    <a href="{{route('advert.all')}}" class="menu_text">
        <div class="menu-item"><a href="{{route('advert.all')}}" class="menu_text">Объявления</a></div>
    </a>
    <div class="menu-item"><a href="{{url('/parsingControll')}}" class="menu_text">Управление парсингом</a></div>
    <a href="{{route('form_keywords')}}" class="menu_text">
        <div class="menu-item"><a href="{{route('form_keywords')}}" class="menu_text">Добавить позиции</a></div>
    </a>
    <div class="menu-item"><a class="menu_text" href="{{url('test')}}">В разработке</a></div>
</div>
