@include('layouts.main')
<style>
    .main_table {
        margin-top: 3%;
        width: 100%;
        display: flex;
    }

    .main_table>.table {
        margin-left: 10%;
        margin-right: 10%;
    }
    td.my-td {
        padding-top: 19px;
        font-size: 20px;
    }
    th.my-th {
        padding-top: 19px;
        font-size: 20px;
    }

    button.btn.btn-deactivate {
        background-color: #6fcfb0;
        color: white;
        text-shadow: -1px 1px 0px #00000094;
    }
    .col12.text-center.paginate {
        margin-left: 44%;
    }
    div#top-paginate {position: absolute;margin-top: .6%;}

    .row {
        position: relative;
    }

</style>
<div class="row">
    <div class="col12 text-center paginate" id="top-paginate">
        {{$keyWords->links()}}
    </div>
</div>

<div class="main_table">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Ключевое слово</th>
            <th scope="col">count</th>
            <th scope="col">Категория</th>
            <th scope="col">Минимальная цена</th>
            <th scope="col">Максимальная цена</th>
            <th scope="col">Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($keyWords as $keyword)
        <tr class="my-Tr">
            <th class="my-th keyword_id" scope="row">{{$keyword->id}}</th>
            <td class="my-td"><a href="{{route('show_adverts_by_keyword_id',['keyword_id' => $keyword->id])}}">{{$keyword->keyword}}</a></td>
            <td class="my-td">{{$keyword->advertsCount}}</td>
            <td class="my-td"><a href="{{$keyword->categories->url}}">{{$keyword->categories->name}}</a></td>
            <td class="my-td">{{$keyword->price_min}}</td>
            <td class="my-td">{{$keyword->price_max}}</td>
            <td>
                <a href="{{route('form_update_keyword',['id' => $keyword->id])}}"><button type="button" class=" mybtn btn btn-primary">Изменить</button></a>
                <button type="button" data-id = "{{$keyword->id}}" class="mybtn btn btn-danger">Удалить</button>
                <button type="button" data-deactivate_id="{{$keyword->id}}" class="btn {{$keyword->deleted_at ===null ? 'btn-warning' : 'btn-deactivate'}}">
                    {{$keyword->deleted_at ===null ? 'Деактивировать' : 'Активировать'}}
                </button>

            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

</div>

<div class="row">
    <div class="col12 text-center paginate">
        {{$keyWords->links()}}
    </div>
</div>

<script>

    $.ajaxSetup({

        headers: {
            'X-CSRF-TOKEN': $("input[name=_token]").val()
        }

    });

    $("button.mybtn.btn.btn-danger").click(function(e){
        let delete_id = $(this).attr('data-id');
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/delete_keywords',
            data: {
                delete_id: delete_id,
            },

            success: function (response) {
                console.log('success');
            },
       });
        $(this).closest(".my-Tr").remove();
        return false;
    });

    $(document).on('click',"button.btn.btn-warning",function (e) {
        $(this).removeClass('btn-warning').addClass('btn-deactivate');
        $(this).text('Активировать');
        let deactivate_id = $(this).attr('data-deactivate_id');
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/activate_deactivate_keyword',
            data: {
                type: 'deactivate',
                deactivate_id: deactivate_id,
            },

            success: function (response) {
                console.log('success');
            },
        });
    });

    $(document).on('click',"button.btn.btn-deactivate",function (e) {
        $(this).removeClass('btn-deactivate').addClass('btn-warning');
        $(this).text('Деактивировать');
        let activate_id = $(this).attr('data-deactivate_id');
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/activate_deactivate_keyword',
            data: {
                type: 'activate',
                activate_id: activate_id,
            },

            success: function (response) {
                console.log('success');
            },
        });
    });



</script>
