<style>
    .not_found-imager {
        display: flex;
        justify-content: center;
        margin-top: 10%;
    }

    .return.back {
        text-align: center;
        font-size: 32px;
    }
    a {
        text-decoration: none;
    }

</style>


<div class="not_found-imager">
    <img src="{{url('404.png')}}" alt="" class="404">
</div>
<a href="{{url('')}}">
    <div class="return back">
        <span>На главную</span>
    </div>
</a>
