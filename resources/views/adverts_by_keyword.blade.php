@include('layouts.main')

<style>
    .main_table {
        margin-top: 3%;
        width: 100%;
        display: flex;
    }

    .main_table>.table {
        margin-left: 10%;
        margin-right: 10%;
    }

    .imager>img {
        width: 200px;
        height: 150px;
    }

</style>


<div class="main_table">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Заголовок</th>
            <th scope="col">Ссылка</th>
            <th scope="col">Цена</th>
            <th scope="col">Картинка</th>
            {{--            <th scope="col">keyword</th>--}}
            <th scope="col">Категория</th>
            <th scope="col">Ключевое слово</th>
        </tr>
        </thead>
        <tbody>
        @foreach($keywordsAdverts as $advert)
                <tr>
                <th class="my-th" scope="row">{{$advert->id}}</th>
                <td class="my-td">{{$advert->title}}</td>
                <td class="my-td"><a target="blank" class="my-link" href="{{$advert->link}}">Посмотреть</a></td>
                <td class="my-td">{{$advert->price}}</td>
                <td class="my-td">
                    <div class="imager">
                        <img  src="{{$advert->image_url}}" alt="">
                    </div></td>
                <td class="my-td">{{$advert->category}}</td>
                <td class="my-td">{{$advert->keywords->keywordWithPrice}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
<div class="my-row">
    <div class="col12 text-center">
        {{$keywordsAdverts->links()}}
    </div>
</div>
